Steps per unit:
  M92 X80.00 Y80.00 Z80.00 E93.00
Maximum feedrates (mm/s):
  M203 X400.00 Y400.00 Z400.00 E45.00
Maximum Acceleration (mm/s2):
  M201 X5000 Y5000 Z5000 E5000
Acceleration: S=acceleration, T=retract acceleration
  M204 S1000.00 T2000.00
Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum XY jerk (mm/s),  Z=maximum Z jerk (mm/s),  E=maximum E jerk (mm/s)
  M205 S0.00 T0.00 B20000 X20.00 Z20.00 E5.00
Home offset (mm):
  M206 X0.00 Y0.00 Z0.00
Endstop adjustment (mm):
  M666 X0.00 Y0.00 Z0.00
Delta Geometry adjustment:
  M666 A0.00 B0.00 C0.00 E0.00 F0.00 G0.00 R92.50 D219.00 H159.75 P0.00
PID settings:
   M301 P15.34 I1.57 D37.45
ok



These settings were printed when I ran M503 on the geeetech two-in-one-out firmware (2017-07-02)
echo:Steps per unit:
echo:  M92 X80.00 Y80.00 Z80.00 E93.00
echo:Maximum feedrates (mm/s):
echo:  M203 X400.00 Y400.00 Z400.00 E45.00
echo:Maximum Acceleration (mm/s2):
echo:  M201 X5000 Y5000 Z5000 E5000
echo:Accelerations: P=printing, R=retract and T=travel
echo:  M204 P1000.00 R2000.00 T2000.00
echo:Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum XY jerk (mm/s),  Z=maximum Z jerk (mm/s),  E=maximum E jerk (mm/s)
echo:  M205 S0.00 T0.00 B20000 X20.00 Z20.00 E5.00
echo:Home offset (mm):
echo:  M206 X0.00 Y0.00 Z0.00
echo:Endstop adjustment (mm):
echo:  M666 X0.00 Y0.00 Z0.00
echo:Delta settings: L=delta_diagonal_rod, R=delta_radius, S=delta_segments_per_second
echo:  M665 L227.00 R92.00 S200.00
echo:Material heatup parameters:
echo:  M145 M0 H190 B55 F0
echo:  M145 M1 H220 B70 F0
echo:PID settings:
echo:  M301 P22.20 I1.08 D114.00 C1.00
echo:Filament settings: Disabled
echo:Z-Probe Offset (mm):
echo:  M851 Z-2.60



settings found before cyclops update 2018/05:
(the firmware is RichCattell-carbon-rods, determined by differences in preheat temps and welcome message)
reset:
start
echo: External Reset
Marlin 1.0.0
echo: Last Updated: Nov  4 2017 17:14:06 | Author: (RichCattell, Mini Kossel)
Compiled: Nov  4 2017
echo: Free Memory: 2814  PlannerBufferBytes: 1232
READING z_probe_offset FROM EEPROM: -41.000,-25.000,-2.000
echo:Stored settings retrieved
echo:Steps per unit:
echo:  M92 X80.00 Y80.00 Z80.00 E93.00
echo:Maximum feedrates (mm/s):
echo:  M203 X400.00 Y400.00 Z400.00 E45.00
echo:Maximum Acceleration (mm/s2):
echo:  M201 X5000 Y5000 Z5000 E5000
echo:Acceleration: S=acceleration, T=retract acceleration
echo:  M204 S1000.00 T2000.00
echo:Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum XY jerk (mm/s),  Z=maximum Z jerk (mm/s),  E=maximum E jerk (mm/s)
echo:  M205 S0.00 T0.00 B20000 X20.00 Z20.00 E5.00
echo:Home offset (mm):
echo:  M206 X0.00 Y0.00 Z0.00
echo:Endstop adjustment (mm):
echo:  M666 X0.00 Y0.00 Z0.00
echo:Delta Geometry adjustment:
echo:  M666 A0.00 B0.00 C0.00 E0.00 F0.00 G0.00 R92.50 D219.00 H159.75 P0.00
echo:PID settings:
echo:   M301 P15.34 I1.57 D37.45
echo:SD card ok
M503:
echo:Steps per unit:
echo:  M92 X80.00 Y80.00 Z80.00 E93.00
echo:Maximum feedrates (mm/s):
echo:  M203 X400.00 Y400.00 Z400.00 E45.00
echo:Maximum Acceleration (mm/s2):
echo:  M201 X5000 Y5000 Z5000 E5000
echo:Acceleration: S=acceleration, T=retract acceleration
echo:  M204 S1000.00 T2000.00
echo:Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum XY jerk (mm/s),  Z=maximum Z jerk (mm/s),  E=maximum E jerk (mm/s)
echo:  M205 S0.00 T0.00 B20000 X20.00 Z20.00 E5.00
echo:Home offset (mm):
echo:  M206 X0.00 Y0.00 Z0.00
echo:Endstop adjustment (mm):
echo:  M666 X0.00 Y0.00 Z0.00
echo:Delta Geometry adjustment:
echo:  M666 A0.00 B0.00 C0.00 E0.00 F0.00 G0.00 R92.50 D219.00 H159.75 P0.00
echo:PID settings:
echo:   M301 P15.34 I1.57 D37.45
ok
